var Player = function(param , game){
	this.game = game;
	this.isActive = false;
	this.checkers = {};
	this.param = param;
	this.shouldMove = false;
	
	this.activate = function(bool){
		this.isActive = bool;
	}

	this.setChecker = function(coor){
		var checker = new Checker(coor , this.game , this);
		this.checkers[coor[0]+"_"+coor[1]] = checker;
	}

	this.setShouldMove = function(bool){
		this.shouldMove = bool;
		return this;
	}

	this.generateSpan = function(){
		var className = this.param.isFirst? "w":"b";
		var span = document.createElement('span');
		span.classList.add('checker');
		span.classList.add(className);
		span.setAttribute('data-player', this.param.isFirst?1:0);
		return span;
	}

	this.drawCheckers = function(){
		for(var i in this.checkers){
			var y = this.checkers[i].coor[0];
			var x = this.checkers[i].coor[1];
			var span = this.generateSpan();
			var a = document.getElementById('cell_'+y+"_"+x);
			a.appendChild(span);
		}
	}

	this.searchChecker = function(coor){
		if(!coor){
			return false;
		}
		return this.checkers[coor[0]+"_"+coor[1]];
	}
}