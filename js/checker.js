var Checker = function(coor, game, player){
	this.coor = coor;
	this.game = game;
	this.player = player;
	this.isDamka = false;

	this.init = function(){
		this.id = this.idByCoor(coor);
	}

	this.isChecker = function(checker){
		if(!checker){
			return false;
		}
		return checker.id == this.id;
	}

	this.idByCoor = function(coor){
		return coor[0] + "_" + coor[1];
	}

	this.moveTo = function(coor){
		delete this.player.checkers[this.id];
		this.coor = coor;
		this.id = this.idByCoor(coor);
		this.player.checkers[this.id] = this;
	}

	this.getSurrounding = function(i){
		return [
			[-i,-i],
			[ i,-i],
			[-i, i],
			[ i, i],
		];
	}

	this.getFreeSurrounding = function(){
		var surrounding = this.getSurrounding(1);
		return this.getCoorsBySurrounding(surrounding);
	}

	this.showAvailable = function(){
		var surrounding = this.getFreeSurrounding();
		this.addClassToCoor(this.coor);
		for(var i in surrounding){
			this.addClassToCoor(surrounding[i]);		
		}
	}

	this.hideAvailable = function(){
		var surrounding = this.getFreeSurrounding();
		this.removeClassToCoor(this.coor);
		for(var i in surrounding){
			this.removeClassToCoor(surrounding[i]);		
		}	
	}

	this.addClassToCoor = function(coor){
		var y =coor[0];		
		var x =coor[1];		
		$(this.game.table).find('#cell_'+y+"_"+x).addClass('available');
	}

	this.removeClassToCoor = function(coor){
		var y =coor[0];		
		var x =coor[1];		
		$(this.game.table).find('#cell_'+y+"_"+x).removeClass('available');
	}

	this.canMoveTo = function(coor){
		return !this.game.players[0].searchChecker(coor) && !this.game.players[1].searchChecker(coor);
	}

	this.getCoorsBySurrounding = function(s, free){
		var arr = [];
		for(var i in s){
			var incr = s[i];
			var y = this.coor[0]+incr[0];
			var x = this.coor[1]+incr[1];
			if(y >= 0 && x>=0 && y<8 && x<8){
				if(free){
					arr.push([y,x]);
				}else{
					if(!this.game.players[0].searchChecker([y,x]) && !this.game.players[1].searchChecker([y,x])){
						arr.push([y,x]);
					}
				}

			}
		}
		return arr;
	}


	this.canKill = function(coor){
		var s1 = this.getSurrounding(1);
		var s2 = this.getSurrounding(2);
		var near        = this.getCoorsBySurrounding(s1, true);
		var killingarea = this.getCoorsBySurrounding(s2, true);
		var opponent = this.game.getActivePlayer(true);

		var arr = [];

		for(var i in near){ // Iterate over all squares around checker
			var coorN = near[i];
			var opponentChecker = opponent.searchChecker(coorN);

			for(var j in killingarea){ // Iterate over all squares with distance "2"
				var coorK = killingarea[j];

				if(opponentChecker){ 
					debugger;
					if(opponentChecker.isInMiddle(this,coorK)){
						arr.push(opponentChecker.coor);
					}
				}
			}
		}
		return arr.length == 0 ? false : arr;
	}

	this.isInMiddle = function(checker, coorK){
		var yN = checker.coor[0];
		var xN = checker.coor[1];

		var y = this.coor[0];
		var x = this.coor[1];


		var yK = coorK[0];
		var xK = coorK[1];	

		return ((yN+yK)/2 == y && (xN+xK)/2 == x);
	}

	this.init();
}
