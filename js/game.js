var Game = function(params){
	this.board = params.board;
	this.map = {}
	this.table = null;
	this.hasStarted = false;
	this.players = [];
	this.active = true;
	this.activeChecker = null;


	this.start = function(){
		this.renderTable();
		this.setPlayers();
		this.setInitialCoordinates();
		this.drawCheckers();
		this.waitForPlayer();
		this.extendJquery();
	}

	this.setPlayers = function(){
		var player1 = new Player({
			isFirst:true	
		},this);
		var player2 = new Player({
			isFirst:false	
		},this);

		this.players.push(player1);
		this.players.push(player2);
		this.active = this.setActive(true);
	}

	this.setActive = function(bool){
		this.active = bool;
		this.getActivePlayer().activate(true);
		this.getActivePlayer(true).activate(false);
		return this;
	}

	this.getPlayerById = function(bool){
		return bool == "1" ? this.players[0] : this.players[1];
	}

	this.getActivePlayer = function(bool){
		if(bool){
			return this.active?this.players[1] : this.players[0];
		}
		return this.active?this.players[0] : this.players[1];
	}

	this.renderTable = function(){
		this.table = document.createElement('table');
		for(var i = 0; i < 8; i++){
			var tr = document.createElement('tr');
			for(var j =0; j < 8; j++){
				var td = document.createElement('td');
				td.id = "cell_"+i+"_"+j;
				td.setAttribute('data-i',i);
				td.setAttribute('data-j',j);
				tr.appendChild(td);
			}
			this.table.appendChild(tr);
		}
		this.board.appendChild(this.table);	
	}

	this.reRenderTable = function(){
		this.removeTable();
		this.renderTable();
		this.drawCheckers();
		this.waitForPlayer();
	}

	this.removeTable = function(){
		$(this.table).remove();
	}

	this.clearTable = function(){
		$(this.table).find('span.checker').remove();
	}



	this.setInitialCoordinates = function(){
		for(var i = 0; i<3; i++){
			for(var j = 0; j<8; j++){
				if(i%2==0){
					if(j%2==1){
						this.players[0].setChecker([i,j]);
					}
				}else{
					if(j%2==0){
						this.players[0].setChecker([i,j]);
					}
				}
			}	
		}

		for(var i = 7; i>4; i--){
			for(var j = 0; j<8; j++){
				if(i%2==0){
					if(j%2==1){
						this.players[1].setChecker([i,j]);
					}
				}else{
					if(j%2==0){
						this.players[1].setChecker([i,j]);						
					}
				}
			}	
		}

		this.players[1].setChecker([3,4]);
	}

	this.isBlack = function(coor){
		var y = coor[0];
		var x = coor[1];
		if((y%2 == 0 && x%2==1) || (y%2 == 1 && x%2==0)){
			return true;
		}
		return false;
	}

	this.setActiveChecker = function(checker){
		this.activeChecker = checker;
		return this;
	}

	this.drawCheckers = function(){
		this.players[0].drawCheckers();
		this.players[1].drawCheckers();
	}

	this.waitForPlayer = function(){
		this.waitForCheckerClick();
		this.waitForMove();		
	}

	this.waitForMove = function(){
		var that = this;
		$(this.table).find('td').click(function(){
			var td = $(this);
			var y = parseInt($(this).attr('data-i'));
			var x = parseInt($(this).attr('data-j'));
			var coor = [y,x];
			var player = that.getActivePlayer();
			if(!player.shouldMove){
				return;
			}
			var clickChecker = player.searchChecker(coor);
			if(that.activeChecker.isChecker(clickChecker)){
				return false;
			}
			if(!that.activeChecker.canMoveTo(coor)){
				return false;
			}
			if(!game.isBlack(coor)){
				return false;
			}
			that.activeChecker.moveTo(coor);
			that.reRenderTable();
			player.setShouldMove(false);
		});
	}

	this.togglePlayer = function(){

	}

	this.waitForCheckerClick = function(){
		var that = this;
		$(this.table).find('span.checker').click(function(){
			var span = $(this);
			var coor = [span.parent().attr('data-i') , span.parent().attr('data-j')];
			var attr = span.attr('data-player');
			var player = that.getPlayerById(attr);
			if(!player.isActive){
				return false;
			}
			var checker = player.searchChecker(coor);
			if(!checker){
				return false;
			}
			if(!player.shouldMove){	
				player.setShouldMove(true);
				that.setActiveChecker(checker);
				checker.showAvailable();
			}else{
				that.reRenderTable();
				if(checker.isChecker(that.activeChecker)){
					player.setShouldMove(false);					
				}else{
					player.setShouldMove(true);
					that.setActiveChecker(checker);
					checker.showAvailable();
				}
			}
		});
	}

	this.extendJquery = function(){
		$.fn.extend({
			getChecker: function(game){
				var span = $(this);
				var coor = [span.parent().attr('data-i') , span.parent().attr('data-j')];
				var attr = span.attr('data-player');
				var player = game.getPlayerById(attr);
				return player.searchChecker(coor);
			}
		});
	}
}

var board = document.getElementById('board');
var game = new Game({
	board:board,
});
game.start();